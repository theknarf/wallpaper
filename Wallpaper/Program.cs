﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;

namespace Wallpaper
{
    class Program
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern Int32 SystemParametersInfo(UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);

        static void SetWallpaper(String path)
        {
            SystemParametersInfo(0x14, 0, path, 0x01 | 0x02);
        }

        static void Main(string[] args)
        {
            String url, name, path, searchterm = "";
            if (args.Length > 0)
            {
                searchterm = args[0];
            }

            if (Regex.Match(searchterm, @"^http.*\.(png|jpg|bmp|gif)$").Success)
            {
                url = searchterm;
                path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/direct/";
            }
            else
            {
                // Search wallbase
                url = wallbase(searchterm);
                path = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "/wallbase/";
            }

            name = Regex.Match(url, @"\/([^/]*)$").Groups[1].Value;

            // Create Directory and download file
            Directory.CreateDirectory(path);

            if (File.Exists(path + name))
            {
                Console.WriteLine(name + " allredy exists, not downloaded");
            }
            else
            {
                new WebClient().DownloadFile(url, path + name);
                Console.WriteLine(name + " downloaded");
            }
            // Set new wallpaper
            SetWallpaper(path + name);
            Console.WriteLine("New wallpaper set");
        }

        static String wallbase(String term)
        {
            String html = getHTML("http://wallbase.cc/search/" + term);
            MatchCollection links = Regex.Matches(html, @"http:\/\/wallbase.cc\/wallpaper\/([0-9]*)");

            if (links.Count < 1)
            {
                dump(html);
                Console.WriteLine("No links where found");
                Environment.Exit(-1);
            }

            int r = new Random().Next(links.Count);
            String url = links[r].ToString();

            Console.WriteLine("Number of links:" + links.Count);
            Console.WriteLine("Link #" + r + " choosen: " + url);

            String html2 = getHTML(url);

            String imgCode = Regex.Match(html2, @"B\(\'([^']*)\'").Groups[1].Value;
            return decode(imgCode);
        }

        static void dump(String dump)
        {
            File.WriteAllText("dump.txt", dump);
        }

        static string getHTML(string url)
        {
            return new WebClient().DownloadString(url);
        }

        static string decode(string data)
        {
            string b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            char o1, o2, o3;
            int h1, h2, h3, h4, bits, i = 0;
            string enc = "";
            do
            {
                h1 = b64.IndexOf(data.Substring(i++, 1));
                h2 = b64.IndexOf(data.Substring(i++, 1));
                h3 = b64.IndexOf(data.Substring(i++, 1));
                h4 = b64.IndexOf(data.Substring(i++, 1));
                bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;
                o1 = (char)(bits >> 16 & 0xff);
                o2 = (char)(bits >> 8 & 0xff);
                o3 = (char)(bits & 0xff);
                if (h3 == 64) enc += new string(new char[] { o1 });
                else if (h4 == 64) enc += new string(new char[] { o1, o2 });
                else enc += new string(new char[] { o1, o2, o3 });
            } while (i < data.Length);
            return enc;
        }
    }
}
